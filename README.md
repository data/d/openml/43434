# OpenML dataset: BitCoin-Dataset

https://www.openml.org/d/43434

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Why 2017 to 2020?
The BitCoin data is Limited to these 3 years because as everyone Knows: Dec 2017 was the time when BitCoin Prices Skyrocketed! Hence, this duration is Perfect for Predicting future Prices. As the years before 2017 had a low Price ratio which can cause a disturbance in our Prediction Models.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43434) of an [OpenML dataset](https://www.openml.org/d/43434). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43434/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43434/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43434/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

